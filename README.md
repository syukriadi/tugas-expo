# IndonesiaMengoding
# InfoQuiz

## Silahkan buat folder baru seperti pada pengerjaan tugas dengan nama "Quiz3" 
## di dalam folder project Expo/React Native Anda.

## Masukkan file index.js, LoginScreen.js, HomeScreen.js, data.json ke dalam folder tersebut
## Import / panggil index.js di App.js pada root folder project agar bisa ditampilkan pada device/emulator Anda

## Quiz kali ini terdiri dari 3 Soal Utama dan 2 Soal Bonus (Tambahan Soal 1 dan Soal Total Harga)
## Penjelasan dan lokasi soal dapat dicari pada file LoginScreen.js dan HomeScreen.js 
## dengan menggunakan fitur search "#Soal" atau "//?" tanpa petik (")

## Berikut daftar soal pada Quiz kali ini

<!-- //? #Soal No. 1 (10 poin) -- LoginScreen.js -- class LoginScreen
    //? Buatlah sebuah fungsi untuk berpindah halaman hanya jika password yang di input bernilai '12345678' 
    //? dan selain itu, maka akan mengubah state isError menjadi true dan tidak dapat berpindah halaman.

    //? #SoalTambahan (+ 5 poin): kirimkan params dengan key => userName dan value => this.state.userName ke halaman Home, 
    //? dan tampilkan userName tersebut di halaman Home setelah teks "Hai," -->
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
class Login extends Component {
constructor(props){
  super(props);
  this.state={
  username:'',
  password:''
  }
 }
render() {
    return (
      <div>
        <MuiThemeProvider>
          <div>
          <AppBar
             title="Login"
           />
           <TextField
             hintText="Enter your Username"
             floatingLabelText="Username"
             onChange = {(event,newValue) => this.setState({username:newValue})}
             />
           <br/>
             <TextField
               type="12345678"
               hintText="Enter your Password"
               floatingLabelText="Password"
               onChange = {(event,newValue) => this.setState({password:newValue})}
               />
             <br/>
             <RaisedButton label="Submit" primary={true} style={style} onClick={(event) => this.handleClick(event)}/>
         </div>
         </MuiThemeProvider>
      </div>
    );
  }
}
const style = {
 margin: 15,
};
export default Login;

<!-- //? #Soal No 2 (15 poin) -- HomeScreen.js -- class HomeScreen
    //? Buatlah 1 komponen FlatList dengan input berasal dari data.json
    //? dan pada prop renderItem menggunakan komponen ListItem -- ada di bawah --
    //? dan memiliki 2 kolom, sehingga menampilkan 2 item per baris (horizontal) -->
const DARE_DEVIL = require('../../assets/images/dare-devil.jpeg');
const HOUSE_OF_CARDS = require('../../assets/images/house-of-cards.jpeg');
const LUKE_CAGE = require('../../assets/images/luke-cage.jpeg');
const ORANGE_IS_THE_NEW_BLACK = require('../../assets/images/orange-is-the-new-black.jpeg');
const STRANGER_THINGS = require('../../assets/images/stranger-things.jpeg');

const SHOWS = [
  {
    title: 'Daredevil',
    summary: 'When a young boy vanishes, a small town uncovers a mystery involving a secret experiment, terrify supernatural forces, and one strange little girl',
    starring: 'Winona Ryder, David Harbour, Mathew Modine',
    creator: 'The Duffer Brothers',
    image: DARE_DEVIL,
  },
  {
    title: 'House Of Cards',
    summary: 'When a young boy vanishes, a small town uncovers a mystery involving a secret experiment, terrify supernatural forces, and one strange little girl',
    starring: 'Winona Ryder, David Harbour, Mathew Modine',
    creator: 'The Duffer Brothers',
    image: HOUSE_OF_CARDS,
  },
  {
    title: 'Luke Cage',
    summary: 'When a young boy vanishes, a small town uncovers a mystery involving a secret experiment, terrify supernatural forces, and one strange little girl',
    starring: 'Winona Ryder, David Harbour, Mathew Modine',
    creator: 'The Duffer Brothers',
    image: LUKE_CAGE,
  },
  {
    title: 'Orange Is The New Black',
    summary: 'When a young boy vanishes, a small town uncovers a mystery involving a secret experiment, terrify supernatural forces, and one strange little girl',
    starring: 'Winona Ryder, David Harbour, Mathew Modine',
    creator: 'The Duffer Brothers',
    image: ORANGE_IS_THE_NEW_BLACK,
  },
  {
    title: 'Stranger Things',
    summary: 'When a young boy vanishes, a small town uncovers a mystery involving a secret experiment, terrify supernatural forces, and one strange little girl',
    starring: 'Winona Ryder, David Harbour, Mathew Modine',
    creator: 'The Duffer Brothers',
    image: STRANGER_THINGS,
  },
  {
    title: 'Daredevil',
    summary: 'When a young boy vanishes, a small town uncovers a mystery involving a secret experiment, terrify supernatural forces, and one strange little girl',
    starring: 'Winona Ryder, David Harbour, Mathew Modine',
    creator: 'The Duffer Brothers',
    image: DARE_DEVIL,
  },
  {
    title: 'House Of Cards',
    summary: 'When a young boy vanishes, a small town uncovers a mystery involving a secret experiment, terrify supernatural forces, and one strange little girl',
    starring: 'Winona Ryder, David Harbour, Mathew Modine',
    creator: 'The Duffer Brothers',
    image: HOUSE_OF_CARDS,
  },
  {
    title: 'Luke Cage',
    summary: 'When a young boy vanishes, a small town uncovers a mystery involving a secret experiment, terrify supernatural forces, and one strange little girl',
    starring: 'Winona Ryder, David Harbour, Mathew Modine',
    creator: 'The Duffer Brothers',
    image: LUKE_CAGE,
  },
  {
    title: 'Orange Is The New Black',
    summary: 'When a young boy vanishes, a small town uncovers a mystery involving a secret experiment, terrify supernatural forces, and one strange little girl',
    starring: 'Winona Ryder, David Harbour, Mathew Modine',
    creator: 'The Duffer Brothers',
    image: ORANGE_IS_THE_NEW_BLACK,
  },
  {
    title: 'StrangerThings',
    summary: 'When a young boy vanishes, a small town uncovers a mystery involving a secret experiment, terrify supernatural forces, and one strange little girl',
    starring: 'Winona Ryder, David Harbour, Mathew Modine',
    creator: 'The Duffer Brothers',
    image: STRANGER_THINGS,
  },
  {
    title: 'Dare Devil',
    summary: 'When a young boy vanishes, a small town uncovers a mystery involving a secret experiment, terrify supernatural forces, and one strange little girl',
    starring: 'Winona Ryder, David Harbour, Mathew Modine',
    creator: 'The Duffer Brothers',
    image: DARE_DEVIL,
  },
  {
    title: 'House Of Cards',
    summary: 'When a young boy vanishes, a small town uncovers a mystery involving a secret experiment, terrify supernatural forces, and one strange little girl',
    starring: 'Winona Ryder, David Harbour, Mathew Modine',
    creator: 'The Duffer Brothers',
    image: HOUSE_OF_CARDS,
  },
  {
    title: 'LukeCage',
    summary: 'When a young boy vanishes, a small town uncovers a mystery involving a secret experiment, terrify supernatural forces, and one strange little girl',
    starring: 'Winona Ryder, David Harbour, Mathew Modine',
    creator: 'The Duffer Brothers',
    image: LUKE_CAGE,
  },
  {
    title: 'Orange Is TheNewBlack',
    summary: 'When a young boy vanishes, a small town uncovers a mystery involving a secret experiment, terrify supernatural forces, and one strange little girl',
    starring: 'Winona Ryder, David Harbour, Mathew Modine',
    creator: 'The Duffer Brothers',
    image: ORANGE_IS_THE_NEW_BLACK,
  },
  {
    title: 'Stranger Things',
    summary: 'When a young boy vanishes, a small town uncovers a mystery involving a secret experiment, terrify supernatural forces, and one strange little girl',
    starring: 'Winona Ryder, David Harbour, Mathew Modine',
    creator: 'The Duffer Brothers',
    image: STRANGER_THINGS,
  },
];

export default SHOWS;
<!--  //? #Soal No 3 (15 poin) -- HomeScreen.js -- class ListItem
    //? Buatlah styling komponen ListItem, agar dapat tampil dengan baik di device -->
import React from "react";
import { StatusBar } from "react-native";
import { Container, Header, Title, Left, Icon, Right, Button, Body, Content,Text, Card, CardItem } from "native-base";
export default class HomeScreen extends React.Component {
  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("DrawerOpen")}>
              <Icon name="menu" />
            </Button>
          </Left>
          <Body>
            <Title>HomeScreen</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          <Card>
            <CardItem>
              <Body>
                <Text>Chat App to talk some awesome people!</Text>
              </Body>
            </CardItem>
          </Card>
          <Button full rounded dark
            style={{ marginTop: 10 }}
            onPress={() => this.props.navigation.navigate("Chat")}>
            <Text>Chat With People</Text>
          </Button>
          <Button full rounded primary
            style={{ marginTop: 10 }}
            onPress={() => this.props.navigation.navigate("Profile")}>
            <Text>Goto Profiles</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

<!-- //? #Soal Bonus (10 poin) -- HomeScreen.js -- class HomeScreen
    //? Buatlah teks 'Total Harga' yang akan bertambah setiap kali salah satu barang/item di klik/tekan.
    //? Di sini, buat fungsi untuk menambahkan nilai dari state.totalPrice dan ditampilkan pada 'Total Harga'. -->
// Homescreen.js
import React, { Component } from 'react';
import { Button, View, Text } from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';

export default class Homescreen extends Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Home Screen</Text>
          <Button
          title="Go to About"
          onPress={() => this.props.navigation.navigate('About')}
/>
      </View>
    )
  }
}
## Contoh hasil akhir dari aplikasi dapat dilihat pada link berikut (jenis file .gif):      
## https://drive.google.com/file/d/1tRXw8WzYCwQMyHhmGjF0ZUZaVTimKrpg/view?usp=sharing

## Setelah selesai mengerjakan, push pekerjaan Anda ke repository gitlab masing-masing
## Input link commit hasil pekerjaan Anda ke sanbercode.com

## Kerjakan soal semampunya dengan baik dan jujur
## Tidak perlu bertanya kepada trainer terkait soal, cukup dikerjakan sesuai pemahaman peserta
## Peserta diperbolehkan untuk googling, namun tidak diperbolehkan untuk bertanya, berdiskusi, atau mencontek.
## Selama quiz berlangsung, grup diskusi telegram akan dinonaktifkan (peserta tidak dapat mengirimkan pesan atau berdiskusi di grup)

# Selamat mengerjakan